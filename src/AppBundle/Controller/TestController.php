<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

class TestController extends BaseController
{
    public function indexAction($code)
    {
        return new Response($code, $code);
    }
}
