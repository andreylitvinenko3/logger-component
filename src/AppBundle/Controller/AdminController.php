<?php

namespace AppBundle\Controller;

use AppBundle\Document\HttpLog;
use AppBundle\Document\Repository\HttpLogRepository;
use AppBundle\Filter\HttpLogFilter;
use AppBundle\Form\Type\HttpLogFilterType;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends BaseController
{
    public function httpLogListAction(Request $request)
    {
        $filter = new HttpLogFilter();

        $form = $this->createFilterForm(HttpLogFilterType::class, $filter);
        $form->handleRequest($request);

        /** @var HttpLogRepository $repository */
        $repository = $this->container->get('doctrine_mongodb')->getRepository(HttpLog::class);

        $qb = $repository->createFilteredQb($filter);

        $paginator  = $this->get('knp_paginator');

        $options = [
            'defaultSortFieldName' => '_id',
            'defaultSortDirection' => 'DESC',
            'defaultDirection' => 'DESC'
        ];

        $pagination = $paginator->paginate($qb, $request->query->getInt('page', 1), 20, $options);

        return $this->render('@App/Admin/http_log_list.html.twig', [
            'filter' => $filter,
            'form' => $form->createView(),
            'pagination' => $pagination
        ]);
    }
}
