<?php

namespace AppBundle\Controller;

use AppBundle\Container\ContainerWrapper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

abstract class BaseController extends Controller
{
    /**
     * @var ContainerWrapper
     */
    protected $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container->get(ContainerWrapper::class);
    }

    /**
     * @param $type
     * @param null $data
     * @param array $options
     * @return FormInterface
     */
    public function createFilterForm($type, $data = null, array $options = [])
    {
        $options = array_merge($options, [
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);

        /** @var FormBuilderInterface $builder */
        $builder = $this->container->get('form.factory')->createNamedBuilder('', $type, $data, $options);
        $builder->setMethod('GET');

        return $builder->getForm();
    }
}
