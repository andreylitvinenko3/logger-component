<?php

namespace AppBundle\Filter;

class HttpLogFilter
{
    protected $ip;

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }
}
