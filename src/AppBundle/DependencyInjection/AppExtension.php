<?php

namespace AppBundle\DependencyInjection;


use AppBundle\HttpLogger\EventSubscriber;
use AppBundle\HttpLogger\HttpLoggerManager;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

class AppExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if (!$config['logger']['enabled']) {
            return;
        }

        $def = new Definition(EventSubscriber::class, [
            new Reference(HttpLoggerManager::class),
            $config['logger']['parameter']
        ]);
        $def->addTag('kernel.event_subscriber');

        $container->setDefinition(EventSubscriber::class, $def);
    }
}
