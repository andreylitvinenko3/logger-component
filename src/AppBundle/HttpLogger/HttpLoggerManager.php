<?php

namespace AppBundle\HttpLogger;

use AppBundle\Document\HttpLog;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HttpLoggerManager
{
    /**
     * @var DocumentManager
     */
    protected $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function log(Request $request, Response $response)
    {
        $log = $this->build($request, $response);

        $this->dm->persist($log);
        $this->dm->flush($log);
    }

    protected function build(Request $request, Response $response)
    {
        $httpLog = new HttpLog();

        $httpRequest = new \AppBundle\Document\Request();
        $httpRequest
            ->setBody($request->getContent())
            ->setHeaders($request->headers->all())
        ;

        $httpResponse = new \AppBundle\Document\Response();
        $httpResponse
            ->setBody($response->getContent())
            ->setHeaders($response->headers->all())
        ;

        $httpLog
            ->setUrl($request->getUri())
            ->setIp($request->getClientIp())
            ->setRequest($httpRequest)
            ->setResponse($httpResponse)
            ->setResponseHttpCode($response->getStatusCode())
        ;

        return $httpLog;
    }
}
