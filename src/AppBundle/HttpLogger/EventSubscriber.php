<?php

namespace AppBundle\HttpLogger;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class EventSubscriber implements EventSubscriberInterface
{
    /**
     * @var HttpLoggerManager
     */
    protected $manager;

    /**
     * @var Request
     */
    protected $request;

    protected $parameter;

    public function __construct(HttpLoggerManager $manager, $parameter)
    {
        $this->manager = $manager;
        $this->parameter = $parameter;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 64],
            KernelEvents::RESPONSE => ['onKernelResponse'],
            KernelEvents::TERMINATE => ['onKernelTerminate']
        ];
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$this->isSupport($event)) {
            return;
        }

        $this->request = clone $event->getRequest();
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$this->isSupport($event)) {
            return;
        }
    }

    public function onKernelTerminate(PostResponseEvent $event)
    {
        if (!$this->isSupport($event)) {
            return;
        }

        $request = $this->request ?: $event->getRequest();

        $this->manager->log($request, $event->getResponse());
    }

    private function isSupport(KernelEvent $event)
    {
        return $event->isMasterRequest() && $this->parameter && $event->getRequest()->query->has($this->parameter);
    }
}
