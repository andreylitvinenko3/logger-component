<?php

namespace AppBundle\Document\Repository;

use AppBundle\Filter\HttpLogFilter;
use Doctrine\ODM\MongoDB\DocumentRepository;

class HttpLogRepository extends DocumentRepository
{
    public function createFilteredQb(HttpLogFilter $filter)
    {
        $qb = $this->createQueryBuilder();

        if ($filter->getIp()) {
            $qb->field('ip')->equals(new \MongoRegex('/' . preg_quote($filter->getIp()) . '/i'));
        }

        return $qb;
    }
}
