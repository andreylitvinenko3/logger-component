<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(collection="http_logs", repositoryClass="AppBundle\Document\Repository\HttpLogRepository")
 */
class HttpLog
{
    /**
     * @Mongo\Id(strategy="AUTO")
     */
    protected $id;

    /**
     * @Mongo\Field(type="string")
     */
    protected $url;

    /**
     * @Mongo\Field(type="int")
     */
    protected $responseHttpCode;

    /**
     * @Mongo\Field(type="string")
     */
    protected $ip;

    /**
     * @var \DateTime
     * @Mongo\Field(type="date")
     */
    protected $date;

    /**
     * @var Request
     * @Mongo\EmbedOne(targetDocument="Request")
     */
    protected $request;

    /**
     * @var Response
     * @Mongo\EmbedOne(targetDocument="Response")
     */
    protected $response;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getResponseHttpCode()
    {
        return $this->responseHttpCode;
    }

    public function setResponseHttpCode($responseHttpCode)
    {
        $this->responseHttpCode = $responseHttpCode;
        return $this;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setResponse(Response $response)
    {
        $this->response = $response;
        return $this;
    }
}
