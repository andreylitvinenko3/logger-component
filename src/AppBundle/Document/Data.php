<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\MappedSuperclass
 */
abstract class Data
{
    /**
     * @var array
     * @Mongo\Field(type="hash")
     */
    protected $headers;

    /**
     * @Mongo\Field(type="string")
     */
    protected $body;

    public function getHeaders()
    {
        return $this->headers;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function getHeadersAsString()
    {
        $string = '';
        foreach ($this->headers as $key => $data) {
            $string .= sprintf("%s: %s", $key, is_array($data) ? implode(', ', $data) : $data) . "\n";
        }

        return $string;
    }
}
