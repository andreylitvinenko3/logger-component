<?php

namespace AppBundle\Container;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ContainerWrapper
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function has($id)
    {
        return $this->container->has($id);
    }

    public function get($id, $invalidBehavior = ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE)
    {
        return $this->container->get($id, $invalidBehavior);
    }

    public function getParameter($name)
    {
        return $this->container->getParameter($name);
    }

    /**
     * @return \Doctrine\Bundle\MongoDBBundle\ManagerRegistry|object
     */
    public function getMongo()
    {
        return $this->container->get('doctrine_mongodb');
    }

    public function getDocumentManager()
    {
        return $this->getMongo()->getManager();
    }
}
